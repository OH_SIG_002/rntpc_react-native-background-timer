# @react-native-ohos/react-native-background-timer

This project is based on [react-native-background-timer](https://github.com/ocetnik/react-native-background-timer)

## Documentation

- [中文](https://gitee.com/react-native-oh-library/usage-docs/blob/master/zh-cn/react-native-background-timer.md)

- [English](https://gitee.com/react-native-oh-library/usage-docs/blob/master/en/react-native-background-timer.md)

## License

This library is licensed under [The MIT License (MIT)](https://gitee.com/openharmony-sig/rntpc_react-native-background-timer/blob/master/LICENSE)