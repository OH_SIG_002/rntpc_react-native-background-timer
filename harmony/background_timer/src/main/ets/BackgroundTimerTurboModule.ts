/*
*
* Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
* Use of this source code is governed by a MIT license that can be
* found in the LICENSE file
*
*/

import { WorkerTurboModule, RNOHError, Tag } from '@rnoh/react-native-openharmony/ts';
import { TM } from "./generated/ts"
import worker from '@ohos.worker';
import type { WorkerTurboModuleContext } from '@rnoh/react-native-openharmony/ts';

// import { BackgroundTimer } from './BackgroundTimerWorker'

export class BackgroundTimerTurboModule extends WorkerTurboModule implements TM.BackgroundTimerTurboModule.Spec {
  constructor(ctx: WorkerTurboModuleContext) {
    super(ctx);
  }


  start(arg: number) {
    //开个线程
    this.ctx.rnInstance.emitDeviceEvent("backgroundTimer", null)
  }

  stop() {
    //移除线程
  }

  setTimeout(id: number, timeout: number) {
    setTimeout(() => {
      this.ctx.rnInstance.emitDeviceEvent("backgroundTimer.timeout", id)
    }, timeout)
  }
}
